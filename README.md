## Requirements
+ Java
+ Node.js

## Install app

``` sh
git clone git@bitbucket.org:pxkw/study-spring-angular.git
cd study-spring-angular
npm install
```

## Launch app

``` sh
./gralew bootRun
```
Then access [http://localhost:8080]()

## Java unit test (JUnit)

``` sh
./gradlew test
```

## Javascript unit test (Karma)

``` sh
./gradlew jsTest
```

## E2e test (Protractor)

``` sh
./gradlew e2eTest
```

or with specific browser (default is 'phantomjs')

``` sh
BROWSER_NAME=chrome ./gradlew e2eTest
```

## Run all tests

``` sh
npm test
```
