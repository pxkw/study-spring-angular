package ex.thymeleaf.dialect;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class CustomUtilTest {

    @Test
    public void testFormatDate() throws Exception {
        final Date date = new SimpleDateFormat("dd/MM/yy").parse("30/05/2015");
        assertThat(CustomUtil.formatDate(date), is("2015/05/30"));
    }
}