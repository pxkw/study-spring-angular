module.exports = function(config) {
  config.set({
    basePath: '../../..',
    frameworks: ['jasmine'],
    files: [
      'node_modules/protractor/testapp/lib/angular_v1.3.13/angular.min.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'src/main/webapp/js/*.js',
      'src/test/js/*_spec.js'
    ],
    exclude: [ ],
    preprocessors: { },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true
  });
};
