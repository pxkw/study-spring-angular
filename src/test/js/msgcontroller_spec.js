describe('MsgController', function(){
  var scope;
  var httpBackend;

  beforeEach(module('App'));

  beforeEach(inject(function(_$httpBackend_, $controller) {
    httpBackend = _$httpBackend_;
    scope = {};
    $controller('MsgController', {$scope: scope});
  }));

  it('should have msg which server respond', function(){
    httpBackend.expectGET('/msg.json').respond('["Hello"]');
    expect(scope.msg).toBe('Requesting ...');
    httpBackend.flush();
    expect(scope.msg).toBe('Hello');
  });

  it('should have msg Failed when request failed', function(){
    httpBackend.expectGET('/msg.json').respond(500);
    expect(scope.msg).toBe('Requesting ...');
    httpBackend.flush();
    expect(scope.msg).toBe('Failed (500).');
  });
});
