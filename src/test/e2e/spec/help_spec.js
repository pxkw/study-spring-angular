'use strict';

describe('help page', function(){
  beforeEach(function(){
    browser.get('/help');
  });

  it('should show title and message', function(){
    expect(browser.getTitle()).toEqual('Help');
    expect(element(by.css('h1')).getText()).toEqual('Help!');
  });
});
