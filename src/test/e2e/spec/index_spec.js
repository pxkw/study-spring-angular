'use strict';

describe('index page', function(){
  beforeEach(function(){
    browser.get('/');
  });

  it('should show title, message, date and link to help', function(){
    expect(browser.getTitle()).toEqual('Hello');
    expect(element(by.id('date')).getText())
        .toMatch(/^20\d\d\/(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])$/);
    expect(element(by.id('msg')).getText()).toEqual('CAN YOU SEE ME?');
    expect(element(by.css('a#link-to-help')).getText()).toEqual('Help');
  });

  it('should move to /help when help link is clicked', function(){
    element(by.id('link-to-help')).click();
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + '/help');
  });
});
