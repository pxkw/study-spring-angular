exports.config = {
  baseUrl: 'http://localhost:8080',

  capabilities: {
    // 'chrome' or 'phantomjs'
   'browserName': process.env.BROWSER_NAME || 'phantomjs',
   'phantomjs.binary.path': './node_modules/.bin/phantomjs'
  },

  framework: 'jasmine2',

  specs: ['spec/*spec.js'],

  jasmineNodeOpts: {
    defaultTimeoutInterval: 10000
  }
};
