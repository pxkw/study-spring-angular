angular.module('App', [])
.controller('MsgController', function($scope, $http) {
  $scope.msg = "Requesting ...";
  $http.get('/msg.json')
  .then( 
    function(res){
      $scope.msg = res.data[0];
    },
    function(res){
      $scope.msg = "Failed (" +res.status+ ").";
    });
});
