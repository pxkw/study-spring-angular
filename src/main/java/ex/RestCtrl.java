package ex;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestCtrl {

    @RequestMapping(value = "/msg.json", produces = { "application/json"} )
    public String msg() {
        return "[\"CAN YOU SEE ME?\"]";
    }
}
