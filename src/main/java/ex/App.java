package ex;

import ex.thymeleaf.dialect.CustomDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;

@ComponentScan
@Configuration
@EnableAutoConfiguration
public class App{

  @Autowired
  public TemplateEngine templateEngine;

  @Bean
  public TemplateEngine addTemplateEngineDialects(){
    templateEngine.addDialect(new CustomDialect());
    return templateEngine;
  }

  public static void main(String[] args){
    SpringApplication.run(App.class, args);
  }
}

