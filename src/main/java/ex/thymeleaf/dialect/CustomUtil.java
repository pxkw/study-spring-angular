package ex.thymeleaf.dialect;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomUtil {
    public static String formatDate(final Date date){
        return new SimpleDateFormat("yyyy/MM/dd").format(date);
    }
}
